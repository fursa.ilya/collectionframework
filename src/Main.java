import com.fursa.collections.arraylist.List;
import com.fursa.collections.arraylist.MyArrayList;
import com.fursa.collections.linkedlist.Linked;
import com.fursa.collections.linkedlist.MyLinkedList;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
        MyLinkedList<String> myLinkedList = new MyLinkedList<>();
        myLinkedList.addLast("1");
        myLinkedList.addLast("2");
        myLinkedList.addLast("3");
        myLinkedList.addLast("4");
        myLinkedList.addLast("5");
        myLinkedList.addLast("6");

        System.out.println("Ascending iterator");

        for (String s: myLinkedList) {
            System.out.print(s + " ");
        }

        System.out.println();
        System.out.println("Descending iterator");

        Iterator iterator = myLinkedList.descendingIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }

    }

}
