package com.fursa.collections.arraylist;

public interface List<E> extends Iterable<E> {

    boolean add(E e);

    void delete(int pos);

    E get(int pos);

    int size();

    void update(int pos, E e);
}
