package com.fursa.collections.arraylist;

import java.util.Iterator;

public class MyArrayList<E> implements List<E> {
    private E[] values;

    public MyArrayList() {
        values = (E[]) new Object[0];
    }

    @Override
    public boolean add(E e) {
        try {
            E[] tmp = values;
            values = (E[]) new Object[tmp.length + 1];
            System.arraycopy(tmp, 0, values, 0, tmp.length);
            values[values.length - 1] = e;
            return true;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public void delete(int pos) {
       try {
           E[] tmp = values;
           values = (E[]) new Object[tmp.length - 1];
           System.arraycopy(tmp, 0, values, 0, pos);
           int elementsAfterIndex = tmp.length - pos - 1;
           System.arraycopy(tmp, pos + 1, values, pos, elementsAfterIndex);
       } catch (ClassCastException ex) {
           ex.printStackTrace();
       }
    }

    @Override
    public E get(int pos) {
        return values[pos];
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public void update(int pos, E e) {
        values[pos] = e;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayIterator<>(values);
    }
}
