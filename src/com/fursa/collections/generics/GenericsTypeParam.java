package com.fursa.collections.generics;

public class GenericsTypeParam<T extends String> {
    private T param;

    public void setParam(T t) {
        param = t;
    }

    public T getParam() {
        return param;
    }
}
