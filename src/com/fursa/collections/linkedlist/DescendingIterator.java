package com.fursa.collections.linkedlist;

import java.util.Iterator;

public interface DescendingIterator<E> {
    Iterator<E> descendingIterator();
}
